# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath(''))

# -- Project information -----------------------------------------------------
project = 'Software Requirements Specification'
copyright = '2022, srsdoc Ltd'
author = 'DK'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.githubpages',
    'sphinxcontrib.confluencebuilder',
    'myst_parser',
    'sphinx.ext.napoleon'
#    'sphinxcontrib.plantuml',
#    'sphinxcontrib.diagrams'
#    'sphinxcontrib.drawio'
]
source_suffix = {
    '.rst':'restructuredtext',
    '.txt':'restructuredtext',
    '.md':'markdown'
}
#drawio_default_transparency = True
#drawio_headless = True
#drawio_no_sandbox = True
#drawio_binary_path = None

#plantuml = 'java -jar ./utils/plantum.jar'
#plantuml = 'java -jar /usr/share/plantuml/plantum.jar'
#plantuml = 'java -jar /opt/plantuml/plantuml.jar'

#https://srsdoc.atlassian.net/wiki/spaces/DSM
confluence_publish = True
#confluence_ask_password = False
confluence_page_hierarchy = True
confluence_disable_autogen_title = False #True
#confluence_publish_dryrun = True
confluence_parent_page = 'SRSDOC'
#confluence_space_key = '~618eaa960faed3006bdb701e'
confluence_space_key = '~62ff48d50268bddf1c9299f5'
#confluence_space_key = 'ALADDIN'
#confluence_ask_password = True
# (for Confluence Cloud)
confluence_server_url = 'https://ca-315df.atlassian.net/wiki/'
confluence_server_user = 'cadimout@yandex.ru'
confluence_server_pass = os.environ.get('SRSDOC_CONFLUENCE_TOKEN')

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
